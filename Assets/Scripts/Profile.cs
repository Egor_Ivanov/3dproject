﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public static class Profile
{
    [System.Serializable]
    public class MainData
    {
        public List<int> LevelStars = new List<int>();
        public int Money = 10;
    }
    
    [System.Serializable]
    public class PlayerData
    {
        public bool sound = true;
        public bool music = true;
    }

    private static MainData mainData;
    private static PlayerData playerData;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CheckData()
    {
        CheckMainData();
        CheckPlayerData();
    }

    private static void CheckMainData()
    {
        Debug.Log("Init profile main data");
        if (mainData != null)
        {
            return;
        }

        mainData = GetData<MainData>("MainData");
    }
    
    private static void CheckPlayerData()
    {
        Debug.Log("Init profile player data");
        if (playerData != null)
        {
            return;
        }

        playerData = GetData<PlayerData>("PlayerData");
    }

    private static T GetData<T>(string key) where T : new()
    {
        if (!PlayerPrefs.HasKey(key))
        {
            return JsonUtility.FromJson<T>(PlayerPrefs.GetString(key));
        }
        
        var data = new T();
        PlayerPrefs.SetString(key, JsonUtility.ToJson(data));
            
        return data;
    }

    public static void Save(bool main = true, bool player = true)
    {
        if (main)
        {
            PlayerPrefs.SetString("MainData", JsonUtility.ToJson(mainData));
        }

        if (player)
        {
            PlayerPrefs.SetString("PlayerData", JsonUtility.ToJson(playerData));
        }
    }

    public static int Money
    {
        get
        {
            return mainData.Money;
        }
        set
        {
            mainData.Money = value;
            if (mainData.Money < 0)
            {
                mainData.Money = 0;
            }
            
            Save(player: false);
        }
    }

    public static int OpenedLevelsCount => mainData.LevelStars.Count;

    public static int GetLevelStars(int level)
    {
        if (level >= mainData.LevelStars.Count)
        {
            return -1;
        }

        return mainData.LevelStars[level];
    }

    public static void SetLevelStars(int level, int stars)
    {
        if (level > mainData.LevelStars.Count)
        {
            Debug.LogError($"Level {level} was not opened");
            
            return;
        }

        if (level == mainData.LevelStars.Count)
        {
            stars = Mathf.Clamp(stars, 0, 3);
            mainData.LevelStars.Add(stars);
            Save(player: false);
            
            return;
        }

        if (stars > mainData.LevelStars[level])
        {
            stars = Mathf.Clamp(stars, 0, 3);
            mainData.LevelStars[level] = stars;
            Save(player: false);
        }
    }
}
