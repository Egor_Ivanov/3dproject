﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private Rigidbody rig;
    [SerializeField] private float speed;
    [SerializeField] private int damage = 1;
    
    private float _baseSpeed;

    private void Reset()
    {
        rig = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        _baseSpeed = speed;
    }

    private void FixedUpdate()
    {
        var velocityNormalized = rig.velocity.normalized;
        rig.velocity = Vector3.Lerp(rig.velocity, velocityNormalized * speed, Time.deltaTime * 5f);
    }

    private void OnCollisionEnter(Collision other)
    {
        var health = other.gameObject.GetComponent<Health>();
        if (health)
        {
            health.SetDamage(damage);
        }
    }
}
