﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public enum CharacterState
{
    Idle,
    Move,
    Attack,
    Skill,
    Hit,
    Dead,
}

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject _ballPrefab;
    [SerializeField] private Transform _firePosition;
    [SerializeField] private int _skillBallsCount = 3;
    [SerializeField] private float _skillAngleBound = 45f;
    [SerializeField] private Transform _deadShotPoint;

    [Header("Bonuses")] 
    [SerializeField] private GameObject hat;
    
    private CharacterState _characterState;
    private bool _move = true;

    private void Reset()
    {
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        _characterState = CharacterState.Idle;

        InputController.OnInputAction += OnInputCommand;
        
        DelayRun.Execute(() => _move = false, 2f, gameObject);
        StartCoroutine(MovementProcess());

        hat.SetActive(false);

        ObjectsPool.Instance.AddObjects(_ballPrefab, 10);
    }
        
    private void OnDestroy()
    {
        InputController.OnInputAction -= OnInputCommand;
    }

    public void AttackEvent()
    {
        var obj = ObjectsPool.Instance.GetObject(_ballPrefab, _firePosition.position, Quaternion.identity);

        var rig = obj.GetComponent<Rigidbody>();
        if (rig)
        {
            rig.velocity = Vector3.zero;
            rig.AddForce(Vector3.forward * 250f, ForceMode.Force);
        }
    }

    public void SkillEvent()
    {
        var angle = _skillAngleBound * 2f / (_skillBallsCount - 1);
        
        for (int i = 0; i < _skillBallsCount; i++)
        {
            var rotation = Quaternion.Euler(0f, _skillAngleBound  - i * angle, 0f);
            var obj = ObjectsPool.Instance.GetObject(_ballPrefab, _firePosition.position, rotation);
            
            transform.Translate(obj.transform.forward * 0.3f);
            
            var rig = obj.GetComponent<Rigidbody>();
            if (rig)
            {
                rig.velocity = Vector3.zero;
                rig.AddForce(obj.transform.forward * 250f, ForceMode.Force);
            }
        }
    }

    public void WearHat()
    {
        hat.SetActive(true);
    }
    
    private IEnumerator MovementProcess()
    {
        _characterState = CharacterState.Move;
        _animator.SetInteger("Run", 1);
        
        while (_move)
        {
            transform.Translate(transform.forward * Time.deltaTime);

            yield return null;
        }

        _characterState = CharacterState.Idle;
        _animator.SetInteger("Run", 0);
    }    

    private void OnInputCommand(InputCommand inputCommand)
    {
        switch (inputCommand)
        {
            case InputCommand.Fire:
                Attack();
                break;
            case InputCommand.Skill:
                Skill();
                break;
            case InputCommand.DeadShot:
                DeadShot();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(inputCommand), inputCommand, null);
        }
    }

    private void Attack()
    {
        if (_characterState == CharacterState.Attack || _characterState == CharacterState.Skill)
        {
            return;
        }
        
        _animator.SetTrigger("Attack");
        _characterState = CharacterState.Attack;
        
        DelayRun.Execute(() => _characterState = CharacterState.Idle, 1.291f, gameObject);
    }

    private void Skill()
    {
        if (_characterState == CharacterState.Attack || _characterState == CharacterState.Skill)
        {
            return;
        }
        
        _animator.SetTrigger("Skill");
        _characterState = CharacterState.Skill;
        
        DelayRun.Execute(() => _characterState = CharacterState.Idle, 0.5f, gameObject);
    }
    
    private void DeadShot()
    {
        if (_characterState == CharacterState.Attack || _characterState == CharacterState.Skill)
        {
            return;
        }
        
        _animator.SetTrigger("Skill");
        _characterState = CharacterState.Skill;

        DelayRun.Execute(() => _characterState = CharacterState.Idle, 1.65f, gameObject);

        DelayRun.Execute(() =>
        {
            var hits = Physics.RaycastAll(_deadShotPoint.position, _deadShotPoint.forward);
            if(!hits.Any())
            {
                return;
            }

            hits = hits.Take(3).ToArray();

            foreach (var hit in hits)
            {
                var heath = hit.transform.GetComponent<Health>();
                if (heath != null)
                {
                    heath.SetDamage(int.MaxValue);
                }    
            }
        }, 1f, gameObject);
    }
}
