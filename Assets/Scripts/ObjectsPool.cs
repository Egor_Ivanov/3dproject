﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public class ObjectsPool : MonoBehaviour
{
    private static ObjectsPool _instance;

    public static ObjectsPool Instance
    {
        get
        {
            if (_instance != null)
            {
                return _instance;
            }
            
            var obj = new GameObject("ObjectsPool");
            _instance = obj.AddComponent<ObjectsPool>();

            return _instance;
        }
    }

    private Dictionary<GameObject, List<GameObject>> pool = new Dictionary<GameObject, List<GameObject>>();

    public GameObject GetObject(GameObject prefab)
    {
        if (!pool.ContainsKey(prefab))
        {
            var obj = Instantiate(prefab);
            
            pool[prefab] = new List<GameObject>{ obj, };
            
            obj.SetActive(true);

            return obj;
        }

        var objects = pool[prefab];
        foreach (var obj in objects)
        {
            if (obj != null && !obj.activeSelf)
            {
                obj.SetActive(true);
                return obj;
            }
        }

        var newObj = Instantiate(prefab);
        newObj.SetActive(true);
        objects.Add(newObj);

        return newObj;
    }

    public GameObject GetObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        var obj = GetObject(prefab);
        obj.transform.position = position;
        obj.transform.rotation = rotation;

        return obj;
    }

    public void AddObjects(GameObject prefab, uint count)
    {
        var objects = new List<GameObject>();
        for (int i = 0; i < count; i++)
        {
            var obj = Instantiate(prefab);
            obj.SetActive(false);
            objects.Add(obj);
        }

        if (!pool.ContainsKey(prefab))
        {
            pool[prefab] = objects;
        }
        else
        {
            pool[prefab].AddRange(objects);
        }
    }
}
