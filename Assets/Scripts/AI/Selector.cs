﻿using System;
using UnityEngine;

namespace AI
{
    public class Selector : Node
    {
        [SerializeField] private Node[] nodes;
        
        public override NodeStates Evaluate()
        {
            foreach (var node in nodes)
            {
                switch (node.Evaluate())
                {
                    case NodeStates.Success:
                        NodeState = NodeStates.Success;
                        return NodeState;
                    case NodeStates.Failure:
                        continue;
                    case NodeStates.Running:
                        NodeState = NodeStates.Running;
                        return NodeState;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            NodeState = NodeStates.Failure;
            return NodeState;
        }
    }
}
