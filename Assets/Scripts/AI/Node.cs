﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public enum NodeStates
    {
        Success,
        Failure,
        Running,
    }

    public abstract class Node : MonoBehaviour
    {
        protected NodeStates NodeState;

        public abstract NodeStates Evaluate();
    }
}
