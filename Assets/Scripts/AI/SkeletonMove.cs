using UnityEngine;

namespace AI
{
    public class SkeletonMove : Node
    {
        [SerializeField] private Transform root;
        [SerializeField] private Animator animator;
        [SerializeField] private int speed = 1;
        
        public override NodeStates Evaluate()
        {
            animator.SetInteger("Movement", speed);
            root.Translate(Vector3.back * Time.deltaTime * speed, Space.World);

            return NodeStates.Success;
        }
    }
}