using System.Collections;
using UnityEngine;

namespace AI
{
    public class SkeletonAttack : Node
    {
        [SerializeField] private Animator animator;

        private Coroutine attackProcessCoroutine;
        
        public override NodeStates Evaluate()
        {
            if (attackProcessCoroutine != null)
            {
                return NodeStates.Running;
            }

            var player = FindObjectOfType<PlayerController>();
            if (!player)
            {
                return NodeStates.Failure;
            }

            if (Vector3.Distance(transform.position, player.transform.position) > 1)
            {
                return NodeStates.Failure;
            }

            attackProcessCoroutine = StartCoroutine(AttackProcess());

            return NodeStates.Success;
        }

        private IEnumerator AttackProcess()
        {
            animator.SetTrigger("Attack");
            
            yield return  new WaitForSeconds(1);

            attackProcessCoroutine = null;
        }
    }
}