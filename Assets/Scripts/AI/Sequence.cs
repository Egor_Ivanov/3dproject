using System;
using UnityEngine;

namespace AI
{
    public class Sequence : Node
    {
        [SerializeField] private Node[] nodes;
        
        public override NodeStates Evaluate()
        {
            var anyChildRunning = false;
            foreach (var node in nodes)
            {
                switch (node.Evaluate())
                {
                    case NodeStates.Success:
                        continue;
                    case NodeStates.Failure:
                        NodeState = NodeStates.Failure;
                        return NodeState;
                    case NodeStates.Running:
                        anyChildRunning = true;
                        continue;
                    default:
                        throw new ArgumentOutOfRangeException();
                }                
            }

            NodeState = anyChildRunning ? NodeStates.Running : NodeStates.Success;
            return NodeState;
        }
    }
}