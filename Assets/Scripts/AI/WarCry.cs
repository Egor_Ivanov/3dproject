using System;
using System.Collections;
using UnityEngine;

namespace AI
{
    public class WarCry : Node
    {
        [SerializeField] private Animator animator;
        
        private enum State
        {
            Wait,
            WarCry,
            Complete,
        }

        private State state;
        private Coroutine coroutine;

        public override NodeStates Evaluate()
        {
            switch (state)
            {
                case State.Wait:
                    state = State.WarCry;
                    coroutine = StartCoroutine(WarCryProcess());
                    return NodeStates.Running;
                case State.WarCry:
                    if (coroutine != null)
                    {
                        return NodeStates.Running;
                    }
                    else
                    {
                        state = State.Complete;
                        return NodeStates.Success;
                    }
                case State.Complete:
                    return NodeStates.Failure;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IEnumerator WarCryProcess()
        {
            animator.SetTrigger("WarCry");
            
            yield return new WaitForSeconds(1.5f);

            coroutine = null;
        }
    }
}