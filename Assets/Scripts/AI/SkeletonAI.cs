using UnityEngine;

namespace AI
{
    public class SkeletonAI : MonoBehaviour
    {
        [SerializeField] private Node rootNode;

        private void Update()
        {
            rootNode.Evaluate();
        }
    }
}