namespace AI
{
    public class CheckOutOfField : Node
    {
        public override NodeStates Evaluate()
        {
            return transform.position.z > 6 ? NodeStates.Running : NodeStates.Failure;
        }
    }
}