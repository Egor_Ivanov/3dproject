﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    [SerializeField] private Transform targetPoint;
    [SerializeField] private float speed = 45f;

    private void Start()
    {
        transform.RotateAround(targetPoint.position, targetPoint.up, speed * Time.deltaTime);
    }
}
