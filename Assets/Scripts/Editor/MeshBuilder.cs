﻿using UnityEditor;
using UnityEngine;

public class MeshBuilder : EditorWindow
{
   [MenuItem("Tools/MeshBuilder/Add noise to mesh")]
   private static void AddNoise()
   {
      var obj = Selection.activeGameObject;
      if (obj == null)
      {
         return;
      }
      
      var meshFilter = obj.GetComponent<MeshFilter>();

      if (meshFilter == null)
      {
         Debug.LogError("No mesh filter in selection");
         return;
      }

      var vertices = meshFilter.sharedMesh.vertices;
      for (int i = 0; i < vertices.Length; i++)
      {
         var pos = vertices[i];
         pos.x += Random.Range(-0.1f, 0.1f);
         pos.z += Random.Range(-0.1f, 0.1f);
         pos.y += Random.Range(-0.1f, 0.1f);
         vertices[i] = pos;
      }

      meshFilter.sharedMesh.vertices = vertices;
   }
}
