﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Target
{
    Player,
    Ball,
}

public class HeadTracking : MonoBehaviour
{
    [SerializeField] private Target target;
    [SerializeField] private Vector3 fixAngle;

    private GameObject _targetObj;

    private void Start()
    {
        if (target == Target.Player)
        {
            _targetObj = FindObjectOfType<PlayerController>().gameObject;
        }
    }

    private void LateUpdate()
    {
        transform.rotation = Quaternion.LookRotation(_targetObj.transform.position - transform.position);

        transform.Rotate(fixAngle);
    }
}
