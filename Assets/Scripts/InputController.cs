﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum InputCommand
{
    Fire,
    Skill,
    DeadShot,
}

public class InputController : MonoBehaviour
{
    [SerializeField] private Button _fireButton;
    [SerializeField] private Button _skillButton;
    [SerializeField] private Button _deadShotButton;
    
    public static event Action<InputCommand> OnInputAction;

    private void Awake()
    {
        _fireButton.onClick.AddListener(OnFireAction);
        _skillButton.onClick.AddListener(OnSkillButton);
        _deadShotButton.onClick.AddListener(OnDeadShotButton);
    }

    private void OnFireAction()
    {
        OnInputAction?.Invoke(InputCommand.Fire);
    }

    private void OnSkillButton()
    {
        OnInputAction?.Invoke(InputCommand.Skill);
    }

    private void OnDeadShotButton()
    {
        OnInputAction?.Invoke(InputCommand.DeadShot);
    }
}
