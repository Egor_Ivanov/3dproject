﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class Bonus : MonoBehaviour
{
    protected void Reset()
    {
        var rig = GetComponent<Rigidbody>();
        rig.isKinematic = true;

        var sphereCollider = GetComponent<SphereCollider>();
        sphereCollider.isTrigger = true;
        sphereCollider.radius = 0.56f;
    }

    protected void Update()
    {
        transform.Translate(Time.deltaTime * 2f * Vector3.back, Space.World);
    }

    protected  void OnMouseDown()
    {
        PickUpBonus();
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player)
        {
            PickUpBonus();
        }
    }

    protected  virtual void PickUpBonus()
    {
        StartCoroutine(MoveUp());
        var sphereCollider = GetComponent<SphereCollider>();
        if (sphereCollider)
        {
            sphereCollider.enabled = false;
        }
    }

    private IEnumerator MoveUp()
    {
        var height = 0f;
        while (height < 10f)
        {
            height += Time.deltaTime * 10f;
            var pos = transform.position;
            pos.y = height;
            transform.position = pos;
            transform.eulerAngles.Set(0, transform.eulerAngles.y * Time.deltaTime * 100f,0);

            yield return null;
        }

        Destroy(gameObject);
    }
}
